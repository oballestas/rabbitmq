import pika

# Conectándose a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))

# Creando un canal
channel = connection.channel()

# Creando Exchange
channel.exchange_declare(
    exchange='procesos_oscar',
    exchange_type='fanout'
)

queue = channel.queue_declare(
    queue='',
    exclusive=True
)
channel.queue_bind(
    queue=queue.method.queue,
    exchange='procesos_oscar'
)


def result_process(ch, method, properties, body):
    print(f"Mensaje Recibido{body}")


# Creando un consumidor en el canal
channel.basic_consume(
    queue=queue.method.queue,
    auto_ack=True,
    on_message_callback=result_process,
)

print("Esperando mensaje")
channel.start_consuming()
