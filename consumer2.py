import pika

# Conectándose a RabbitMQ
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
# cred = pika.PlainCredentials(
#     'urnlmxke',
#     'I-LXldBBXqlNToL0e5IT9fCtDlLEY7dP',
# )
#
# connection = pika.BlockingConnection(pika.ConnectionParameters(
#     host='jaguar.rmq.cloudamqp.com',
#     virtual_host='urnlmxke',
#     credentials=cred,
# ))

# Creando un canal
channel = connection.channel()

# Creando una cola dentro del canal
channel.queue_declare(
    queue='procesos_oscar',
)

channel.queue_declare(
    queue='resultados_oscar',
)


def message_processor(ch, method, properties, body):
    print("Mensaje Recibido")
    print(ch, method, properties, body)
    ch.basic_publish(
        exchange='',
        routing_key='resultados_oscar',
        body=bytes(f'Respuesta del mensaje {body}', encoding='utf-8')
    )


# Creando un consumidor en el canal
channel.basic_consume(
    queue='procesos_oscar',  # Especificando qué cola del canal voy a consumir
    auto_ack=False,  # Automáticamente responder que el mensaje ha sido recibido
    on_message_callback=message_processor,  # Especificando qué función se va a ejecutar con el contenido del mensaje
)

print("Esperando mensaje")
channel.start_consuming()
