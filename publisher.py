import pika

# # Creando credencial
# cred = pika.PlainCredentials(
#     'urnlmxke',
#     'I-LXldBBXqlNToL0e5IT9fCtDlLEY7dP',
# )
#
# connection = pika.BlockingConnection(pika.ConnectionParameters(
#     host='jaguar.rmq.cloudamqp.com',
#     virtual_host='urnlmxke',
#     credentials=cred,
# ))

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.exchange_declare(
    exchange='procesos_oscar',
    exchange_type='fanout'
)

# channel.queue_declare(
#     queue='procesos_oscar'
# )
#
# channel.queue_declare(
#     queue='resultados_oscar'
# )

# Publicando un mensaje
msg = b'Test bidireccional-Oscar'
channel.basic_publish(
    exchange='procesos_oscar',
    routing_key='',
    body=msg  # Esto es un byte, no un string
)
print('Mensaje Enviado', msg)

# Escuchando Resultados

# def result_process(ch, method, properties, body):
#     print(f"Resultado Recibido:{body}")
#     ch.basic_ack(delivery_tag=method.delivery_tag)
#
#
# channel.basic_consume(
#     queue='resultados_oscar',
#     auto_ack=False,
#     on_message_callback=result_process,
# )

# print('Esperando resultados')
# channel.start_consuming()
# connection.close()
